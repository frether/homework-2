#!/usr/bin/env python

from nose.tools import assert_equal
import aims as ai

def test_1():
    List=[5,4,3]
    obs = ai.std(List)
    exp = 0.816496580927726
    assert_equal(obs, exp)
def test_2():
    List = [-5,3,-11]
    obs = ai.std(List)
    exp = 5.734883511361751
    assert_equal(obs, exp)
def test_3():
    List = [-1,0,1,3]
    obs = ai.std(List)
    exp = 1.479019945774904
    assert_equal(obs, exp)
def test_4():
    List = [8,-10]
    obs = ai.std(List)
    exp = 9.0
    assert_equal(obs, exp)
def test_5():
    List = ['data/bert/audioresult-00215','data/bert/audioresult-00235']
    obs = ai.avg_range(List)
    exp = 7.5
    assert_equal(obs, exp)
def test_6():
    List = ['data/bert/audioresult-00416', 'data/bert/audioresult-00532']
    obs = ai.avg_range(List)
    exp = 6.5
    assert_equal(obs, exp)
def test_7():
    List = ['data/bert/audioresult-00222', 'data/bert/audioresult-00380']
    obs = ai.avg_range(List)
    exp = 7.0
    assert_equal(obs, exp)
