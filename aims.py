#!/usr/bin/env python


def mean(numbers):
       return sum(numbers) / float(len(numbers))  
    
def std(x):
	s=0
	for i in range(len(x)):
		s=s+(x[i]-mean(x))**2
	return (s/float(len(x)))**0.5

def avg_range(x):
	avg=[]
	for i in x:
		File=open(i)
		for line in File:
			data=line.strip().split(": ")
			if data[0]=="Range":
				avg.append(int(data[1]))
	return float(sum(avg))/float(len(avg))

def main():
    print "welcome to AIMS module"

if __name__ == "__main__":
    main()    
